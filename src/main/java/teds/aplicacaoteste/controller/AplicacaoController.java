package teds.aplicacaoteste.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/app")
public class AplicacaoController {
	
	@PostMapping("/ola")
	public String darBoasVindas(String nome, Model model) {
		if (nome.isBlank()) {
			nome = "João Ninguém";
		}
		model.addAttribute("nome", nome);
		return "mostrarboasvindas";
	}

}
