FROM eclipse-temurin:17
VOLUME /tmp
ADD /target/*.jar aplicacaoteste-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/aplicacaoteste-0.0.1-SNAPSHOT.jar"]
